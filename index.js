Vue.component('table-row', {
    props: ['index', 'start', 'end'],
    template: `<tr>
        <td>{{index}}</td>
        <td><input class="form-control border-0" v-bind:value="start" v-on:input="$emit('input', {index: index-1, period: 'start', value: $event.target.value})"></td>
        <td><input class="form-control border-0" v-bind:value="end" v-on:input="$emit('input', {index: index-1, period: 'end', value: $event.target.value})"></td>
    </tr>`
});

var vm = new Vue({
    el: '#vm',
    data: {
        _input: null,
        displayResult: true,
        hours: 0
    },
    computed: {
        days:  function(){
            this._days = this._days ?? [];
            for(var i = 1; i <= 31; i++) {
                this._days.push({day: i, start: 0, end: 0});
            }
            return this._days;
        },
        input: {
            get: function() {
                return this._input;
            },
            set: function(inputValue) {
                var day = this._days[inputValue['index']];
                var period = inputValue['period'];
                var value = inputValue['value']
                if(period == 'start') {
                    day['start'] = parseInt(value);
                } else {
                    day['end'] = parseInt(value);
                }
            }
        }
    },
    methods: {
        contHours: function() {
            this.hours = this._days.reduce((acc, item)=>acc+(item['end']-item['start']), 0);
        }
    }
});